
# -*- coding:utf-8 -*-	
#Author		:	Sam
#Data		: 	Jul 27 2015
#Version	:	0.01	Design the main frames
#			:	0.02	Add Searcher	
#			:	0.03	Add search option
		

import Pmw,re,thread,zlib,os,fileinput
from Pmw import *
import cPickle as pickle
from time import ctime,sleep
from Tkinter import *
from SimpleDialog import *
import Tkinter as tk 
#from PIL import Image,ImageTk



def compress(fileName):
	infile = open(fileName,"rb")
	outfile = open(fileName+".dat","wb")

	compress = zlib.compressobj()
	data = infile.read()

	outfile.write(compress.compress(data))
	outfile.write(compress.flush())
	infile.close()
	outfile.close()

def uncompress(fileName):
	infile = open(fileName,"rb")

	decompress = zlib.decompressobj()
	data = infile.read()
	infile.close()

	str = decompress.decompress(data)
	return str

class file:
    def __init__(self,name,path):
        self._name = name
        self._path = path

    def __init__(self,str):
    	self._name = str.split('/')[-1]
    	self._path = str[:-1*len(self._name)]
    	#print self._name

    def getName(self):
        return self._name

    def getLowerName(self):
        return self._name.lower()

    def getPath(self):
        return self._path

class symbol:
    def __init__(self,name,type,linenum,path,description):
        self._name = name
        self._type = type
        self._linenum = linenum
        self._path = path
        self._description = description

    def __init__(self,str):
    	pass

    def getName(self):
        return self._name

    def getLowerName(self):
        return self._name.lower()

    def getType(self):
        return self._type

    def getLinenum(self):
        return self._linenum

    def getPath(self):
        return self._path

    def getDescription(self):
        return self._description

class Cache:
	def __init__(self,searchText,searchOption,searchSource,searchResult):
		self._searchText = searchText
		self._searchOption = searchOption
		self._searchSource = searchSource
		self._searchResult = searchResult




class Searcher:
	def __init__(self,app,timeout=20):
		self._timeout = timeout
		self._finishFlag = False
		self._searchType = 'unknown'
		self._result = ""
		self._app = app

		self._option = {"CASESENSTIVE":False,"WHOLEWORLD":False,"FILE":False,"SYMBOL":False,"ALL":False}
		self._currentWorkList = []
		
		#search result
		self._file_found = 0
		self._symbol_found = 0
		self._is_searching = False

		self._files = []
		self._symbols = []

		self._searchSource = "Ax3.1"
		self.setRadioAll()

	def notifyMsg(self,msg):
		self._app.display_message(msg)
    	
	def debug(self,str):
		rpt = "%s" % (92*'#'+'\n')
		rpt = "%s" % (rpt+"It's a debug messge:"+str+'\n')
		self.notifyUpdate(rpt)	

	def setCehckCasesenstive(self):
		if self._option["CASESENSTIVE"] == False:
			self._option["CASESENSTIVE"] = True
			str = "True"
		elif self._option["CASESENSTIVE"] == True:
			self._option["CASESENSTIVE"] = False
			str = "False"
		

	def setCheckWholeword(self):
		if self._option["WHOLEWORLD"] == False:
			self._option["WHOLEWORLD"] = True
			str = "True"
		elif self._option["WHOLEWORLD"] == True:
			self._option["WHOLEWORLD"] = False
			str = "False"
		

	def setRadioFile(self):
		self._option["FILE"] = True
		self._option["SYMBOL"] = False
		self._option["ALL"] = False

	def setRadioSymbol(self):
		self._option["FILE"] = False
		self._option["SYMBOL"] = True
		self._option["ALL"] = False

	def setRadioAll(self):
		self._option["FILE"] = False
		self._option["SYMBOL"] = False
		self._option["ALL"] = True

	def search(self,str):
		if self._is_searching == False:
			self._app.freeze()
			self._file_found = 0
			self._symbol_found = 0
			self._app.display_message("Now searching for you , please wait ......")
			self._searchStr = str
			self._is_searching = True
			thread.start_new_thread(self.doSearch,())

		
	def doSearch(self):
		try:
			if self._option["FILE"] == True:
				self.searchFile()
			elif self._option["SYMBOL"] == True:
				self.searchSymbol()
			elif self._option["ALL"] == True:
				self.searchFile()
				self.searchSymbol()
		except:
			pass

		self._app.display_search_result(self._file_found,self._symbol_found)
		self._app.unfreeze()
		self._is_searching = False
		#self._app.result.flush()
		thread.exit_thread()

	def judgeType(self,judgeStr):
		if judgeStr.find(".") == -1:
			self._searchType = 'unknown'
		else:
			self._searchType = 'file'

	def notifyUpdate(self,strs):
		self._app.updateResult(strs)

	def searchFile(self):
		path = "data/"+self._searchSource+"/files"
		#lines = uncompress(path)
		#print lines
		for line in fileinput.input(path):
		#for line in lines:
			fileName = line.split('/')[-1]
			fileName = fileName.split('\n')[0]
			find_flag = False
			if self._option["WHOLEWORLD"] == True and self._option["CASESENSTIVE"] == True:
				if fileName == self._searchStr:
					find_flag = True
			elif self._option["WHOLEWORLD"] == True and self._option["CASESENSTIVE"] == False:
				fileName = fileName.lower()
				searchStr = self._searchStr.lower()
				if fileName == searchStr:
					find_flag = True
			elif self._option["WHOLEWORLD"] == False and self._option["CASESENSTIVE"] == True:
				if self._searchStr in fileName:
					find_flag = True			
			elif self._option["WHOLEWORLD"] == False and self._option["CASESENSTIVE"] == False:
				fileName = fileName.lower()
				searchStr = self._searchStr.lower()
				if searchStr in fileName:
					find_flag = True
			if find_flag == True:
				self._file_found += 1
				rpt = "%s" % ('\n'+80*'#'+'\n'+line)
				self.notifyUpdate(rpt)
				sleep(0.1)
			

	def searchSymbol(self):
		path = "data/"+self._searchSource+"/"
		header = self._searchStr[0:1].upper()
		source = path+header+".s4u"
		#if header in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
			#print header
		lines = uncompress(source)
		lines = lines.split('\n')

		#for line in fileinput.input(sources):
		for line in lines:
			if line == "":
				continue

			match = re.findall("\s+",line)
			symbolName = line.split(match[0])[0]
			
			find_flag = False

			if self._option["WHOLEWORLD"] == True and self._option["CASESENSTIVE"] == True:
				if symbolName == self._searchStr:
					find_flag = True
			elif self._option["WHOLEWORLD"] == True and self._option["CASESENSTIVE"] == False:
				symbolName = symbolName.lower()
				searchStr = self._searchStr.lower()
				if symbolName == searchStr:
					find_flag = True
			elif self._option["WHOLEWORLD"] == False and self._option["CASESENSTIVE"] == True:
				if self._searchStr in symbolName:
					find_flag = True
			elif self._option["WHOLEWORLD"] == False and self._option["CASESENSTIVE"] == False:
				symbolName = symbolName.lower()
				searchStr = self._searchStr.lower()
				if searchStr in symbolName:
					find_flag = True

			#print line,symbolName,searchStr
			if find_flag == True:

				self._symbol_found +=1
				symbolName = line.split(match[0])[0]
				rpt = "%s" % ('\n'+80*'#'+'\n')
				rpt = "%s" % (rpt+"Name : "+symbolName+'\n')


				index = len(symbolName+match[0])
				substr = line[index:]
				match = re.findall("\s+",substr)
				stype = substr.split(match[0])[0]
				rpt = "%s" % (rpt+"Type : "+stype+'\n')


				index = len(stype+match[0])
				substr = substr[index:]
				match = re.findall("\s+",substr)
				lineNum = substr.split(match[0])[0]
				rpt = "%s" % (rpt+"Line Num : "+lineNum+'\n')


				index = len(lineNum+match[0])
				substr = substr[index:]
				match = re.findall("\s+",substr)
				path = substr.split(match[0])[0]
				rpt = "%s" % (rpt+"Path : "+path+'\n')


				index = len(path+match[0])
				code = substr[index:]
				rpt = "%s" % (rpt+"Code : "+code+'\n')

				
				self.notifyUpdate(rpt)
				sleep(0.1)
			
			

class Application(): 
	def __init__(self, master=None):
		self._search = Searcher(app=self,timeout=10)
		self._master = master
		self.createWidgets()
		self._RadioAll.select()
		#self.freeze()
		self.display_message("Ready to search ...")
		

	def createWidgets(self):
		#logo
		#photo = ImageTk.open("logo.jpg") 
		#image = Image.open("logo.jpg")
		#image = image.resize((100,80),Image.ANTIALIAS)
		#photo = ImageTk.PhotoImage(image)
		photo = PhotoImage(file="logo.gif")
		self._log = tk.Button(master=self._master,image=photo)
		self._log.image = photo
		self._log['width'] = 60
		self._log['height'] = 20
		self._log.grid(row=0,column=0,rowspan=3,sticky=W+E+N+S)
		#print image.format,image.size,image.mode

		#search option
		radioVal = IntVar()
		self._WholeWord = tk.Checkbutton(master=self._master,text="Whole Word",command=self.select_check_wholeword)
		self._WholeWord.grid(row = 0 ,column = 1 ,sticky = W)
		self._CaseSenstive = tk.Checkbutton(master=self._master,text="Case Senstive",command=self.select_check_casesenstive)
		self._CaseSenstive.grid(row = 0 ,column = 2,sticky = W)
		self._RadioFile = tk.Radiobutton(master=self._master,text="Search  File",variable=radioVal,value=1,command=self.select_radio_file)
		self._RadioFile.grid(row = 0 ,column = 3,sticky = W)
		self._RadioSymbol = tk.Radiobutton(master=self._master,text="Search Symbol",variable=radioVal,value=2,command=self.select_radio_sybmol)
		self._RadioSymbol.grid(row = 0 ,column = 4 ,sticky = W)
		self._RadioAll = tk.Radiobutton(master=self._master,text="Search All",variable=radioVal,value=3,command=self.select_radio_all)
		self._RadioAll.grid(row = 0 ,column = 5,sticky = W)


		#search info
		self.e = StringVar()
		self._searchText = tk.Entry(master=self._master,width=85,textvariable=self.e)
		self._searchText.grid(row = 1, column = 1,sticky = W,columnspan=5)
		self.e.set("Please input you search text here~")
		self._searchText.bind('<Button-1>',self.enter)
		self._searchText.bind('<Return>',self.doSearch)
		#self._prevButton = Button(master=self._master,text="<<===")
		#self._prevButton.grid(row=1,column=1,columnspan=1)
		#self._nextButton = Button(master=self._master,text="===>>")
		#self._nextButton.grid(row=1,column=5,columnspan=1)

		#search state
		self._stateStr = StringVar()
		self._progressNum = 0
		self._state = tk.Label(master=self._master,textvariable=self._stateStr,justify='left')
		self._state.grid(row=2,column=1,sticky=W,columnspan=3)

		#search source
		self.datas = os.listdir("data")
		self._sourceComboBox = Pmw.ComboBox(self._master,selectioncommand=self.source_selected,scrolledlist_items=self.datas,history=0)
		self._sourceComboBox.grid(row=2,column=4,columnspan=2)
		self.load_search_source()
		self._sourceComboBox.selectitem(0)
		
		self._lock = thread.allocate()
	
		#result info
		Pmw.initialise()
		self.result = Pmw.ScrolledText(self._master,text_height=38)
		self.result.grid(row=3,column=0,sticky=W+E+N+S,columnspan=6,rowspan=2)


	def source_selected(self,data):
		self._searchText._searchSource = data
		self._search._searchSource = data


	def load_search_source(self):
		self.datas = os.listdir("data")
		self._sourceComboBox.clear()
		for elem in self.datas:
			self._sourceComboBox.insert(END,elem)

	def display_message(self,msg):
		self._stateStr.set(msg)

	def display_search_result(self,fileCount,symbolCount):
		rpt = "%s" % ("Search done: "+str(fileCount)+" files and "+str(symbolCount)+" symbols found.")
		self._stateStr.set(rpt)
		

	def display_progress(self,desc,percent):
		if self._lock.acquire():
			rpt = desc+percent+self._progressNum*'*'
			self._stateStr.set(rpt)
			self._lock.release()

	def freeze(self):
		self._searchText["state"] = DISABLED
		self._WholeWord["state"] = DISABLED
		self._CaseSenstive["state"] = DISABLED
		self._RadioFile["state"] = DISABLED
		self._RadioSymbol["state"] = DISABLED
		self._RadioAll["state"] = DISABLED
		#self._prevButton["state"] = DISABLED
		#self._nextButton["state"] = DISABLED
		#slef._sourceComboBox["state"] = DISABLED

	def unfreeze(self):
		self._searchText["state"] = NORMAL
		self._WholeWord["state"] = NORMAL
		self._CaseSenstive["state"] = NORMAL
		self._RadioFile["state"] = NORMAL
		self._RadioSymbol["state"] = NORMAL
		self._RadioAll["state"] = NORMAL
		#self._prevButton["state"] = NORMAL
		#self._nextButton["state"] = NORMAL
		#slef._sourceComboBox["state"] = NORMAL

	def select_radio_file(self):
		#self._search.debug("select_radio_file")
		self._search.setRadioFile()
		
	def select_radio_sybmol(self):
		#self._search.debug("select_radio_sybmol")
		self._search.setRadioSymbol()

	def select_radio_all(self):
		#self._search.debug("select_radio_all")
		self._search.setRadioAll()

	def select_check_wholeword(self):
		#self._search.setCheckWholeword()
		self._search.setCheckWholeword()

	def select_check_casesenstive(self):
		self._search.setCehckCasesenstive()

	def enter(self,event):
		if self.e.get() == "Please input you search text here~":
			self.e.set("")
			
	def doSearch(self,event):
		 self.result.clear()
		 searchStr = self.e.get()
		 self._search.search(searchStr)

	def updateResult(self,str):
		if self._lock.acquire():
			self.result.insert(END,str)	
			self._lock.release()

if __name__ == '__main__':
	app = Application(Tk()) 
	app._master.title('Search For You') 
	app._master.minsize(580,580)
	app._master.maxsize(880,880)
	app._master.mainloop() 
