#!/usr/bin/python

import zlib,sys,os

def compress(fileName):
	infile = open(fileName,"rb")
	outfile = open(fileName+".s4u","wb")

	compress = zlib.compressobj()
	data = infile.read()

	outfile.write(compress.compress(data))
	outfile.write(compress.flush())
	infile.close()
	outfile.close()


if __name__ == '__main__':
	path = sys.argv[1]+"/"
	for file in os.listdir(path):
		compress(path+file)
