	Release note Ver 0.03

	* Function
		- Add search history,press the button to select the previous or next search information
		- Add search source
		- Add code preview(according to notepad++ mini)
		- Add search source online updating 	
		- Add a server to collect search info
		- Add edit function to the search result(copy,paste,etc)
		- Add a lexical and syntax analysis program to analysis PL/SQL
		- Add a function to highlight keyword in search result

	* Performance
		- Optimize the search procedure by alphabet separation.	
	
	* Bug fix
		- Fix much script bugs in Tool

#########################################################################################

	Release note Ver 0.02

	* Function
		- Add search option , now you can search in "Whole Word" "Case Sensitive" etc
		- Add a label to display the search status
		- Re-design the search work flow
		- Add freeze and unfreeze for GUI 
		
	* Performance
		- Load big file with paging algorithm
		
	* Bug fix
		- Fix a bug that caused by multi-thread conflict may lose search result.